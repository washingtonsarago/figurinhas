﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Figurinhas.Domain.Entities
{
    public class Figurinha
    {
        public int FigurinhaId { get; set; }

        [Display(Name = "Número da Figurinha")]
        [Required(ErrorMessage = "Campo obrigatório", AllowEmptyStrings = false)]
        public int NumeroDaFigurinha { get; set; }

        [Display(Name = "Quantidade")]
        [Required(ErrorMessage = "Campo obrigatório", AllowEmptyStrings = false)]
        public int Total { get; set; }

        [Display(Name = "Qtd Solicitada")]
        public int Reservada { get; set; }

        public int UsuarioId { get; set; }

        [ForeignKey("UsuarioId")]
        public Usuario Usuario { get; set; }

        public bool isAtivo { get; set; }

    
    }
}
