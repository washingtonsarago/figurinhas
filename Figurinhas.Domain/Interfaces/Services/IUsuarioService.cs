﻿using Figurinhas.Domain.Entities;
using System.Collections.Generic;

namespace Figurinhas.Domain.Interfaces.Services
{
    public interface IUsuarioService : IServiceBase<Usuario>
    {
        Usuario VerificarUsuarioESenha(string usuario, string senha);

        Usuario VerificarSeUsuarioEstadisponivel(string usuario);

        List<Usuario> RankingTrocas();
    }
}
