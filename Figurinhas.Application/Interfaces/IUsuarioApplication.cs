﻿using Figurinhas.Domain.Entities;
using System.Collections.Generic;

namespace Figurinhas.Application.Interfaces
{
    public interface IUsuarioApplication : IApplicationBase<Usuario>
    {
        Usuario VerificarUsuarioESenha(string usuario, string senha);

        bool VerificarSeUsuarioEstadisponivel(string usuario);

        List<Usuario> RankingTrocas();
    }
}
