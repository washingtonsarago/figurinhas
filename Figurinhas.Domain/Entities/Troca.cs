﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Figurinhas.Domain.Entities
{
    public class Troca
    {
        public int TrocaId { get; set; }
        public int Quantidade { get; set; }
        public int UsuarioEntradaId { get; set; }
        public int UsuarioSaidaId { get; set; }
        public int StatusTroca { get; set; }

        public int FigurinhaEntradaId { get; set; }
        public int FigurinhaSaidaId { get; set; }


        [ForeignKey("FigurinhaEntradaId")]
        public Figurinha FigurinhaEntrada { get; set; }

        [ForeignKey("FigurinhaSaidaId")]
        public Figurinha FigurinhaSaida { get; set; }


        [ForeignKey("UsuarioEntradaId")]
        public Usuario UsuarioEntrada { get; set; }

        [ForeignKey("UsuarioSaidaId")]
        public Usuario UsuarioSaida { get; set; }

    }
}
