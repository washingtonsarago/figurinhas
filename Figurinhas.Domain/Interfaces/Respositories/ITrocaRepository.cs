﻿using Figurinhas.Domain.Entities;

namespace Figurinhas.Domain.Interfaces.Respositories
{
    public interface ITrocaRepository : IRepositoryBase<Troca>
    {
    }
}
