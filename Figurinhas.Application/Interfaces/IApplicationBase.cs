﻿using System.Collections.Generic;

namespace Figurinhas.Application.Interfaces
{
    public interface IApplicationBase<TEntity> where TEntity : class
    {
        void Add(TEntity obj);

        void Update(TEntity obj, object parameters);
        TEntity GetById(long id);
        IEnumerable<TEntity> GetAll();
        void Update(TEntity obj);
        void Remove(TEntity obj);
        void Dispose();
    }
}
