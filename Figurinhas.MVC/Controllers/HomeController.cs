﻿using Figurinhas.Application.Interfaces;
using Figurinhas.Domain.Entities;
using Ninject;
using System;
using System.Web.Mvc;

namespace Figurinhas.MVC.Controllers
{
    public class HomeController : Controller
    {
        [Inject]
        public IUsuarioApplication usuarioApplication { get; set; }

        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Msg = (string)TempData["Data"];
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            try
            {
                string usuario = collection["username"];
                string senha = collection["password"];

                if (string.IsNullOrEmpty(usuario) && string.IsNullOrEmpty(senha))
                {
                    return View();
                }

                Usuario user = usuarioApplication.VerificarUsuarioESenha(usuario, senha);
                if (user == null)
                {
                    ViewBag.Msg = "Usuário ou senha incorretos";
                    return View();
                }

                Session.Add("Usuario", user);
                return RedirectToActionPermanent("Index", "Portal");
            }
            catch (Exception ex)
            {
                ViewBag.Msg = ex.Message;
                return View();
            }

        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Cadastrar(FormCollection collection)
        {
            string usuario = collection["username"];
            string senha = collection["password"];

            try
            {
                if (!usuarioApplication.VerificarSeUsuarioEstadisponivel(usuario))
                    TempData["Data"] = "Usuário já foi utilizado.";

                else
                {
                    Usuario user = new Usuario()
                    {
                        Nome = usuario,
                        Senha = senha
                    };
                    usuarioApplication.Add(user);
                }
            }

            catch (Exception ex)
            {
                TempData["Data"] = ex.Message;
            }

            return RedirectToAction("Index", "Home");
        }

    }
}