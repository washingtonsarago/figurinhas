﻿using System.Collections.Generic;

namespace Figurinhas.Domain.Interfaces.Services
{
    public interface IServiceBase<TEntity> where TEntity : class
    {
        void Add(TEntity obj);
        TEntity GetById(long id);
        void Update(TEntity obj, object parameters);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> GetAllAsNoTraking();
        void Update(TEntity obj);
        void Remove(TEntity obj);
        void Dispose();
    }
}
