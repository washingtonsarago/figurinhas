﻿using Figurinhas.Application.Interfaces;
using Figurinhas.Domain.Entities;
using Figurinhas.Domain.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace Figurinhas.Application
{
    public class FigurinhaApplication : ApplicationBase<Figurinha>, IFigurinhaApplication
    {
        private readonly IFigurinhaService figurinhaService;

        public FigurinhaApplication(IFigurinhaService figurinhaService) : base(figurinhaService)
        {
            this.figurinhaService = figurinhaService;
        }

        public Figurinha BuscarPorNumero(int Numero)
        {
            return figurinhaService.GetAll().Where(f => f.NumeroDaFigurinha == Numero).FirstOrDefault();
        }

        public List<Figurinha> BuscarPorUsuario(int UsuarioId)
        {
            return figurinhaService.GetAll().Where(f => f.UsuarioId == UsuarioId && f.isAtivo).ToList();
        }

        public bool FigurinhaDisponivel(int FigurinhaId)
        {
            var figurinha = figurinhaService.GetById(FigurinhaId);
            if (figurinha.Total - figurinha.Reservada > 1)
                return true;
            else
                return false;
        }

        public List<Figurinha> BuscarFigurinhasDisponiveis(int UsuarioId)
        {
            return figurinhaService.GetAll().Where(f => f.UsuarioId != UsuarioId && f.isAtivo && f.Total - f.Reservada > 1).ToList();
        }

        public Figurinha BuscarPorNumeroEUsuario(int Numero, int UsuarioId)
        {
            return figurinhaService.GetAll().Where(f => f.NumeroDaFigurinha == Numero && f.UsuarioId == UsuarioId).FirstOrDefault();
        }
    }
}
