﻿using Figurinhas.Domain.Interfaces.Respositories;
using Figurinhas.Infra.Data.Contexto;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Figurinhas.Infra.Data.Respositories
{
    public class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    {
        protected FigurinhasContext Db = new FigurinhasContext();

        public virtual void Add(TEntity obj)
        {
            Db.Set<TEntity>().Add(obj);
            Db.SaveChanges();
        }

        public TEntity GetById(long id)
        {
            return Db.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            //using (DapperBase dapper = new DapperBase())
            //{
            //    return dapper.GetAll<TEntity>().ToList();
            //}

            return Db.Set<TEntity>().ToList();
        }

        public IEnumerable<TEntity> GetAllAsNoTraking()
        {
            return Db.Set<TEntity>().AsNoTracking().ToList();
        }

        public IEnumerable<TEntity> GetAllAsNoTracking()
        {
            return Db.Set<TEntity>().AsNoTracking().ToList();
        }

        public void Update(TEntity obj)
        {
            Db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();
        }

        public void Update(TEntity obj, object parameters)
        {
            using (DapperBase dapper = new DapperBase())
            {
                dapper.Update(obj);
            }

            //   Db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
            //  Db.SaveChanges();
        }

        public void Remove(TEntity obj)
        {
            Db.Set<TEntity>().Remove(obj);
            Db.SaveChanges();
        }

        public void Dispose()
        {
            Db.Dispose();
        }
    }
}
