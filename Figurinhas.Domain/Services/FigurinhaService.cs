﻿using Figurinhas.Domain.Entities;
using Figurinhas.Domain.Interfaces.Respositories;
using Figurinhas.Domain.Interfaces.Services;

namespace Figurinhas.Domain.Services
{
    public class FigurinhaService : ServiceBase<Figurinha>, IFigurinhaService
    {
        private readonly IFigurinhaRepository figurinhaRepository;

        public FigurinhaService(IFigurinhaRepository figurinhaRepository) : base(figurinhaRepository)
        {
            this.figurinhaRepository = figurinhaRepository;
        }

    }
}
