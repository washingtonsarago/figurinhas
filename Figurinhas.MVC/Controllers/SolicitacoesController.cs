﻿using Figurinhas.Application.Interfaces;
using Figurinhas.Domain;
using Figurinhas.Domain.Entities;
using Figurinhas.MVC.Models;
using Ninject;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Figurinhas.MVC.Controllers
{
    public class SolicitacoesController : Controller
    {
        [Inject]
        public ITrocaApplication trocaApplication { get; set; }

        [Inject]
        public IUsuarioApplication usuarioApplication { get; set; }

        [Inject]
        public IFigurinhaApplication figurinhaApplication { get; set; }

        // GET: Solicitacoes
        public ActionResult Index()
        {
            Usuario usuario = (Usuario)Session["Usuario"];
            if (usuario == null)
                return RedirectToActionPermanent("Index", "Home");

            List<Usuario> lUsuarios = usuarioApplication.GetAll().ToList();
            List<Troca> lTroca = trocaApplication.BuscarTrocasPendentes(usuario.UsuarioId);
            List<TrocaModel> lTrocaModel = new List<TrocaModel>();
            Figurinha figurinhaSolicitante;
            Figurinha figurinhaSolicitada;
            TrocaModel troca;
            foreach (var item in lTroca)
            {
                figurinhaSolicitada = figurinhaApplication.GetById(item.FigurinhaSaidaId);
                figurinhaSolicitante = figurinhaApplication.GetById(item.FigurinhaEntradaId);
                troca = new TrocaModel
                {
                    NumeroFigurinhaEntrada = figurinhaSolicitada.NumeroDaFigurinha,
                    NumeroFigurinhaSaida = figurinhaSolicitante.NumeroDaFigurinha,
                    FigurinhaSaidaId = item.FigurinhaSaidaId,
                    FigurinhaEntradaId = item.FigurinhaEntradaId,
                    UsuarioSaidaId = lUsuarios.Where(c => c.UsuarioId == item.UsuarioSaidaId).FirstOrDefault().UsuarioId,
                    NomeUsuario = lUsuarios.Where(c => c.UsuarioId == item.UsuarioEntradaId).FirstOrDefault().Nome,
                    Quantidade = item.Quantidade,
                    TrocaId = item.TrocaId
                };
                lTrocaModel.Add(troca);
            }

            return View(lTrocaModel);
        }

        public ActionResult Aceitar(int id)
        {
            Usuario usuarioEntrada = (Usuario)Session["Usuario"];
            if (usuarioEntrada == null)
                return RedirectToActionPermanent("Index", "Home");

            Troca troca = trocaApplication.GetById(id);
            troca.StatusTroca = (byte)StatusTroca.Aceito;
            trocaApplication.Update(troca);

            Figurinha figurinhaSaida = figurinhaApplication.GetById(troca.FigurinhaSaidaId);
            Figurinha figurinhaEntrada = figurinhaApplication.GetById(troca.FigurinhaEntradaId);
            Usuario usuarioSaida = usuarioApplication.GetById(troca.UsuarioEntradaId);
            usuarioEntrada = usuarioApplication.GetById(usuarioEntrada.UsuarioId);
            figurinhaSaida.Reservada--;
            figurinhaEntrada.Reservada--;
            figurinhaSaida.Total--;
            figurinhaEntrada.Total--;

            //figurinhaSaida.Total = troca.Quantidade;
            //figurinhaEntrada.Total = troca.Quantidade;

            //Verificando se o usuario já possui a figurinha
            Figurinha saida = figurinhaApplication.BuscarPorUsuario(troca.UsuarioSaidaId).Where(c => c.NumeroDaFigurinha == figurinhaSaida.NumeroDaFigurinha).FirstOrDefault();
            if (saida != null)
            {
                saida.Total += troca.Quantidade;
                figurinhaApplication.Update(saida);
            }
            else
            {

                Figurinha figuraSaidaAdd = new Figurinha()
                {
                    isAtivo = true,
                    NumeroDaFigurinha = figurinhaSaida.NumeroDaFigurinha,
                    Reservada = 0,
                    Total = 1,
                    UsuarioId = troca.UsuarioSaidaId
                };
                
                figurinhaApplication.Add(figuraSaidaAdd);
            }
            //Verificando o mesmo caso para quem solicitou a troca

            var entrada = figurinhaApplication.BuscarPorUsuario(troca.UsuarioEntradaId).Where(c => c.NumeroDaFigurinha == figurinhaEntrada.NumeroDaFigurinha).FirstOrDefault();

            if (entrada != null)
            {
                entrada.Total += troca.Quantidade;
                figurinhaApplication.Update(entrada);
            }
            else
            {
                Figurinha figuraEntradaAdd = new Figurinha()
                {
                    isAtivo = true,
                    NumeroDaFigurinha = figurinhaEntrada.NumeroDaFigurinha,
                    Reservada = 0,
                    Total = 1,
                    UsuarioId = troca.UsuarioEntradaId
                };

                figurinhaApplication.Add(figuraEntradaAdd);
            }
            usuarioSaida.TotalTrocas++;
            usuarioEntrada.TotalTrocas++;

            usuarioApplication.Update(usuarioSaida);
            usuarioApplication.Update(usuarioEntrada);


            return RedirectToAction("Index");
        }

        public ActionResult Rejeitar(int id)
        {
            Usuario usuario = (Usuario)Session["Usuario"];
            if (usuario == null)
                return RedirectToActionPermanent("Index", "Home");

            Troca troca = trocaApplication.GetById(id);

            Figurinha figurinhaSolicitada = figurinhaApplication.GetById(troca.FigurinhaSaidaId);
            Figurinha figurinhaSolicitante = figurinhaApplication.GetById(troca.FigurinhaEntradaId);

            figurinhaSolicitada.Reservada--;
            figurinhaSolicitante.Reservada--;
            figurinhaApplication.Update(figurinhaSolicitada);
            figurinhaApplication.Update(figurinhaSolicitante);

            trocaApplication.Remove(troca);


            return RedirectToAction("Index");
        }
    }
}