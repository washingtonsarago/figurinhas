﻿using Figurinhas.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Figurinhas.Infra.Data.EntityConfig
{
    public class UsuarioConfiguration : EntityTypeConfiguration<Usuario>
    {
        public UsuarioConfiguration()
        {
            HasKey(u => u.UsuarioId);

            Property(p => p.Nome)
                .IsRequired();

            Property(p => p.Senha)
                .HasMaxLength(50)
                .IsRequired();
        }
    }
}
