﻿using Figurinhas.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Figurinhas.Infra.Data.EntityConfig
{
    public class FigurinhaConfiguration : EntityTypeConfiguration<Figurinha>
    {
        public FigurinhaConfiguration()
        {
            HasKey(f => f.FigurinhaId);

            Property(f => f.NumeroDaFigurinha)
                .IsRequired();

            Property(f => f.Total)
                .IsRequired();

            Property(f => f.Reservada)
                .IsRequired();

            Property(f => f.UsuarioId)
                .IsRequired();
        }
    }
}
