﻿using Figurinhas.Application.Interfaces;
using Figurinhas.Domain;
using Figurinhas.Domain.Entities;
using Figurinhas.MVC.Models;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Figurinhas.MVC.Controllers
{
    public class TrocarController : Controller
    {
        [Inject]
        public IUsuarioApplication usuarioApplication { get; set; }

        [Inject]
        public ITrocaApplication trocaApplication { get; set; }

        [Inject]
        public IFigurinhaApplication figurinhaApplication { get; set; }

        // GET: Trocar
        public ActionResult Index()
        {
            Usuario usuario = (Usuario)Session["Usuario"];
            if (usuario == null)
                return RedirectToActionPermanent("Index", "Home");

            List<Figurinha> lFigurinhas = figurinhaApplication.BuscarFigurinhasDisponiveis(usuario.UsuarioId);
            List<FigurinhaTrocaModel> lFigurinhaTrocaModels = new List<FigurinhaTrocaModel>();
            List<Usuario> lUsuarios = usuarioApplication.GetAll().ToList();
            FigurinhaTrocaModel model;
            foreach (var figurinha in lFigurinhas)
            {
                model = new FigurinhaTrocaModel()
                {
                    FiguraEntradaId = figurinha.FigurinhaId,
                    NumeroFiguraEntrada = figurinha.NumeroDaFigurinha,
                    NomeUsuario = lUsuarios.Where(u => u.UsuarioId == figurinha.UsuarioId).FirstOrDefault().Nome,
                    Quantidade = figurinha.Total - figurinha.Reservada
                };
                lFigurinhaTrocaModels.Add(model);
            }


            return View(lFigurinhaTrocaModels);
        }


        public ActionResult Solicitar(FigurinhaTrocaModel figurinhaTrocaModel)
        {
            Usuario usuario = (Usuario)Session["Usuario"];
            if (usuario == null)
                return RedirectToActionPermanent("Index", "Home");
            List<Figurinha> lFigurinhas = figurinhaApplication.BuscarPorUsuario(usuario.UsuarioId).Where(c => c.Total - c.Reservada > 1).ToList();

            figurinhaTrocaModel.MinhasFigurinhas = lFigurinhas;

            return View(figurinhaTrocaModel);
        }

        [HttpPost]
        public ActionResult Solicitar(string Id, FormCollection collection)
        {
            Usuario usuario = (Usuario)Session["Usuario"];
            if (usuario == null)
                return RedirectToActionPermanent("Index", "Home");
            FigurinhaTrocaModel figurinhaTrocaModel = new FigurinhaTrocaModel(); ;

            if (string.IsNullOrEmpty(collection["FiguraSaidaId"]) || collection["FiguraSaidaId"] == "0")
            {
                figurinhaTrocaModel = new FigurinhaTrocaModel
                {
                    FiguraEntradaId = int.Parse(collection["FiguraEntradaId"]),
                    UsuarioEntradaId = int.Parse(collection["UsuarioEntradaId"]),
                    UsuarioSaidaId = int.Parse(collection["UsuarioSaidaId"]),
                    NumeroFiguraEntrada = int.Parse(collection["NumeroFiguraEntrada"]),
                    NomeUsuario = collection["NomeUsuario"]
                };
                ViewBag.Msg = "Por favor escolha a figurinha que você vai oferecer.";

                List<Figurinha> lFigurinhas = figurinhaApplication.BuscarPorUsuario(usuario.UsuarioId).Where(c => c.Total - c.Reservada > 1).ToList();

                figurinhaTrocaModel.MinhasFigurinhas = lFigurinhas;

                return View(figurinhaTrocaModel);
            }

            try
            {


                Figurinha figurinhaSaida = figurinhaApplication.GetById(int.Parse(collection["FiguraSaidaId"]));
                Figurinha figurinhaEntrada = figurinhaApplication.GetById(int.Parse(collection["FiguraEntradaId"]));

                Troca troca = new Troca()
                {
                    FigurinhaSaidaId = figurinhaSaida.FigurinhaId,
                    FigurinhaEntradaId = figurinhaEntrada.FigurinhaId,
                    Quantidade = 1,
                    UsuarioEntradaId = figurinhaSaida.UsuarioId,
                    UsuarioSaidaId = figurinhaEntrada.UsuarioId,
                    StatusTroca = (byte)StatusTroca.Solicitado
                };
                trocaApplication.Add(troca);

                figurinhaSaida.Reservada += 1;
                figurinhaEntrada.Reservada += 1;

                figurinhaApplication.Update(figurinhaSaida);
                figurinhaApplication.Update(figurinhaEntrada);

                return RedirectToAction("Index");
            }

            catch (Exception ex)
            {
                ViewBag.Msg = ex.Message;
                return View(figurinhaTrocaModel);
            }
        }

    }
}