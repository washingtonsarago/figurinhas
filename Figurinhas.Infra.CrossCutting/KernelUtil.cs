﻿using Ninject;

namespace Figurinhas.Infra.CrossCutting
{
    public static class KernelUtil
    {
        static KernelUtil()
        {
            Kernel = new StandardKernel(new Module());
        }

        public static IKernel Kernel { get; set; }
    }
}
