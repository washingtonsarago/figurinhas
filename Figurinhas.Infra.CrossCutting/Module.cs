﻿using Figurinhas.Application;
using Figurinhas.Application.Interfaces;
using Figurinhas.Domain.Interfaces.Respositories;
using Figurinhas.Domain.Interfaces.Services;
using Figurinhas.Domain.Services;
using Figurinhas.Infra.Data.Respositories;

namespace Figurinhas.Infra.CrossCutting
{
    public class Module : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind(typeof(IApplicationBase<>)).To(typeof(ApplicationBase<>));
            Bind(typeof(IServiceBase<>)).To(typeof(ServiceBase<>));
            Bind(typeof(IRepositoryBase<>)).To(typeof(RepositoryBase<>));

            Bind<IUsuarioApplication>().To<UsuarioApplication>();
            Bind<IUsuarioService>().To<UsuarioService>();
            Bind<IUsuarioRepository>().To<UsuarioRepository>();

            Bind<IFigurinhaApplication>().To<FigurinhaApplication>();
            Bind<IFigurinhaService>().To<FigurinhaService>();
            Bind<IFigurinhaRepository>().To<FigurinhaRepository>();

            Bind<ITrocaApplication>().To<TrocaApplication>();
            Bind<ITrocaService>().To<TrocaService>();
            Bind<ITrocaRepository>().To<TrocaRepository>();

        }
    }
}
