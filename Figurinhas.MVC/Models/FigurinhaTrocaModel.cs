﻿using Figurinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Figurinhas.MVC.Models
{
    public class FigurinhaTrocaModel
    {
        [Display(Name = "Nome do Usuário")]
        public String NomeUsuario { get; set; }

        public int FiguraEntradaId { get; set; }
        [Display(Name = "Número da Figurinha")]
        public int NumeroFiguraEntrada { get; set; }
        [Display(Name = "Figurinha a oferecer")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Campo Requerido")]
        public int FiguraSaidaId { get; set; }
        public int UsuarioEntradaId { get; set; }
        public int UsuarioSaidaId { get; set; }

        public int Quantidade { get; set; }

        public IEnumerable<Figurinha> MinhasFigurinhas { get; set; }


    }
}