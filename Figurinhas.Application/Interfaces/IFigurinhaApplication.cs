﻿using Figurinhas.Domain.Entities;
using System.Collections.Generic;

namespace Figurinhas.Application.Interfaces
{
    public interface IFigurinhaApplication : IApplicationBase<Figurinha>
    {
        bool FigurinhaDisponivel(int FigurinhaId);

        List<Figurinha> BuscarPorUsuario(int UsuarioId);

        Figurinha BuscarPorNumero(int Numero);
        Figurinha BuscarPorNumeroEUsuario(int Numero, int UsuarioId);

        List<Figurinha> BuscarFigurinhasDisponiveis(int UsuarioId);
    }
}
