﻿using Figurinhas.Application.Interfaces;
using Figurinhas.Domain.Entities;
using Figurinhas.Domain.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace Figurinhas.Application
{
    public class UsuarioApplication : ApplicationBase<Usuario>, IUsuarioApplication
    {
        private readonly IUsuarioService usuarioService;

        public UsuarioApplication(IUsuarioService usuarioService) : base(usuarioService)
        {
            this.usuarioService = usuarioService;
        }

        public List<Usuario> RankingTrocas()
        {
            return usuarioService.GetAllAsNoTraking().OrderByDescending(u => u.TotalTrocas).ToList();
        }

        public bool VerificarSeUsuarioEstadisponivel(string usuario)
        {
            Usuario user = usuarioService.VerificarSeUsuarioEstadisponivel(usuario);

            if (user == null)
                return true;
            else
                return false;
        }

        public Usuario VerificarUsuarioESenha(string usuario, string senha)
        {
            return usuarioService.VerificarUsuarioESenha(usuario, senha);
        }
    }
}
