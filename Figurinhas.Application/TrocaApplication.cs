﻿using Figurinhas.Application.Interfaces;
using Figurinhas.Domain;
using Figurinhas.Domain.Entities;
using Figurinhas.Domain.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace Figurinhas.Application
{
    public class TrocaApplication : ApplicationBase<Troca>, ITrocaApplication
    {
        private readonly ITrocaService trocaService;

        public TrocaApplication(ITrocaService trocaService) : base(trocaService)
        {
            this.trocaService = trocaService;
        }

        public List<Troca> BuscarTrocasPendentes(int UsuarioId)
        {
            return trocaService.GetAll().Where(t => t.UsuarioSaidaId == UsuarioId && t.StatusTroca == (byte)StatusTroca.Solicitado).ToList();
        }


        public void SolicitarTroca(Troca troca)
        {
            trocaService.Add(troca);
        }
    }
}
