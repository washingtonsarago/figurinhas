﻿using Figurinhas.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Figurinhas.Infra.Data.EntityConfig
{
    public class TrocaConfiguration : EntityTypeConfiguration<Troca>
    {
        public TrocaConfiguration()
        {
            HasKey(t => t.TrocaId);

            Property(t => t.Quantidade).IsRequired();
            Property(t => t.StatusTroca).IsRequired();
            Property(t => t.UsuarioEntradaId).IsRequired();
            Property(t => t.UsuarioSaidaId).IsRequired();
            Property(t => t.FigurinhaSaidaId).IsRequired();
            Property(t => t.FigurinhaEntradaId).IsRequired();
        }

    }
}
