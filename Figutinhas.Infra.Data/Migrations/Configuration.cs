namespace Figurinhas.Infra.Data.Migrations
{
    using Figurinhas.Domain.Entities;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Figurinhas.Infra.Data.Contexto.FigurinhasContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Figurinhas.Infra.Data.Contexto.FigurinhasContext context)
        {

            //context.UsuarioDbSet.AddOrUpdate(x => x.UsuarioId,
            //new Usuario() { UsuarioId = 1, Nome = "admin", Senha = "admin" },
            //new Usuario() { UsuarioId = 2, Nome = "teste", Senha = "teste" },
            //new Usuario() { UsuarioId = 3, Nome = "novo", Senha = "novo" }
            //);

            //context.FigurinhaDbSet.AddOrUpdate(x => x.FigurinhaId,
            //new Figurinha() { FigurinhaId = 1, UsuarioId = 1, NumeroDaFigurinha = 2, Total = 5, Reservada = 0, isAtivo = true },
            //new Figurinha() { FigurinhaId = 2, UsuarioId = 2, NumeroDaFigurinha = 3, Total = 3, Reservada = 0, isAtivo = true },
            //new Figurinha() { FigurinhaId = 3, UsuarioId = 1, NumeroDaFigurinha = 7, Total = 44, Reservada = 0, isAtivo = true },
            //new Figurinha() { FigurinhaId = 4, UsuarioId = 3, NumeroDaFigurinha = 4, Total = 12, Reservada = 0, isAtivo = true }
            //);

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
