﻿using Figurinhas.Domain.Entities;
using Figurinhas.Domain.Interfaces.Respositories;

namespace Figurinhas.Infra.Data.Respositories
{
    public class TrocaRepository : RepositoryBase<Troca>, ITrocaRepository
    {
    }
}
