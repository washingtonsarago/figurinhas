﻿using System.ComponentModel.DataAnnotations;

namespace Figurinhas.MVC.Models
{
    public class TrocaModel
    {
        public int TrocaId { get; set; }

        public int FigurinhaEntradaId { get; set; }
        public int FigurinhaSaidaId { get; set; }

        [Display(Name = "Dar")]
        public int NumeroFigurinhaSaida { get; set; }
        [Display(Name = "Por")]
        public int NumeroFigurinhaEntrada { get; set; }

        public int Quantidade { get; set; }
        public int UsuarioEntradaId { get; set; }
        public int UsuarioSaidaId { get; set; }

        [Display(Name = "Usuário")]
        public string NomeUsuario { get; set; }

    }
}