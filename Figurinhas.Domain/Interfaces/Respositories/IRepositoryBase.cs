﻿using System;
using System.Collections.Generic;

namespace Figurinhas.Domain.Interfaces.Respositories
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        void Add(TEntity obj);
        TEntity GetById(Int64 id);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> GetAllAsNoTraking();
        void Update(TEntity obj);
        void Update(TEntity obj, object parameters);
        void Remove(TEntity obj);
        void Dispose();
    }
}
