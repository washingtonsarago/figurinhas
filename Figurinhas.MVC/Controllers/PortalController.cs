﻿using Figurinhas.Application.Interfaces;
using Figurinhas.Domain.Entities;
using Ninject;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Figurinhas.MVC.Controllers
{
    public class PortalController : Controller
    {
        [Inject]
        public IUsuarioApplication usuarioApplication { get; set; }



        // GET: Portal
        public ActionResult Index()
        {

            Usuario usuario = (Usuario)Session["Usuario"];
            if (usuario == null)
                return RedirectToActionPermanent("Index", "Home");

            List<Usuario> lUsuarios = usuarioApplication.RankingTrocas();


            return View(lUsuarios);
        }

        public ActionResult Sair()
        {
            Session["Usuario"] = null;
            return RedirectToActionPermanent("Index", "Home");

        }
    }
}