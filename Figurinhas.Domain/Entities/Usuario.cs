﻿using System.ComponentModel.DataAnnotations;

namespace Figurinhas.Domain.Entities
{
    public class Usuario
    {



        public int UsuarioId { get; set; }

        public string Nome { get; set; }

        public string Senha { get; set; }

        [Display(Name = "Total de Trocas")]
        public int TotalTrocas { get; set; }


        /// <summary>
        /// Construtor Padrão
        /// </summary>
        public Usuario()
        {

        }



    }
}
