﻿using Figurinhas.Domain.Entities;
using System.Collections.Generic;

namespace Figurinhas.Application.Interfaces
{
    public interface ITrocaApplication : IApplicationBase<Troca>
    {
        void SolicitarTroca(Troca troca);

        List<Troca> BuscarTrocasPendentes(int UsuarioId);

    }
}
