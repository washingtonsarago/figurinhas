﻿using Figurinhas.Application.Interfaces;
using Figurinhas.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;

namespace Figurinhas.Application
{

    public class ApplicationBase<TEntity> : IDisposable, IApplicationBase<TEntity> where TEntity : class
    {
        private readonly IServiceBase<TEntity> _serviceBase;

        public ApplicationBase(IServiceBase<TEntity> serviceBase)
        {
            _serviceBase = serviceBase;
        }

        public virtual void Add(TEntity obj)
        {
            _serviceBase.Add(obj);
        }

        public TEntity GetById(long id)
        {
            return _serviceBase.GetById(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _serviceBase.GetAll();
        }

        public void Update(TEntity obj)
        {
            _serviceBase.Update(obj);
        }

        public void Update(TEntity obj, object parameters)
        {
            _serviceBase.Update(obj, parameters);
        }

        public void Remove(TEntity obj)
        {
            _serviceBase.Remove(obj);
        }

        public void Dispose()
        {
            _serviceBase.Dispose();
        }
    }
}
