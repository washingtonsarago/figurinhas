﻿using Figurinhas.Domain.Entities;
using Figurinhas.Domain.Interfaces.Respositories;
using Figurinhas.Domain.Interfaces.Services;

namespace Figurinhas.Domain.Services
{
    public class TrocaService : ServiceBase<Troca>, ITrocaService
    {

        private readonly ITrocaRepository trocaRepository;

        public TrocaService(ITrocaRepository trocaRepository) : base(trocaRepository)
        {
            this.trocaRepository = trocaRepository;
        }

      
    }
}
