﻿using System.Collections.Generic;

namespace Figurinhas.Domain.Interfaces.Respositories
{
    public interface IDapperBase
    {
        void ExecuteProcedure(string name);
        void ExecuteProcedure(string name, object parameters);

        List<T> ExecuteProcedure<T>(string name);
        List<T> ExecuteProcedure<T>(string name, object parameters);

        List<T> GetAll<T>();
        List<T> Get<T>(string tableName, object parameters);
        List<T> Get<T>(object parameters);

        T ExecuteProcedureScalar<T>(string name, object parameters);

        bool TesteConexao();
    }
}
