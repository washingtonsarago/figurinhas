﻿using Dapper;
using Figurinhas.Domain.Interfaces.Respositories;
using Figurinhas.Infra.Data.Contexto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Figurinhas.Infra.Data.Respositories
{
    public class DapperBase : IDisposable, IDapperBase
    {


        public int TimeOut { get { return 200; } }

        protected FigurinhasContext Db = new FigurinhasContext();

        protected readonly SqlConnection conexao;

        public DapperBase()
        {
            conexao = new SqlConnection(Db.Database.Connection.ConnectionString);
        }

        public DapperBase(string Conexao)
        {
            conexao = new SqlConnection(Conexao);
        }

        public void ExecuteProcedure(string name)
        {
            conexao.Open();
            conexao.Execute(name, commandType: CommandType.StoredProcedure, commandTimeout: TimeOut);
            conexao.Close();
        }

        public void ExecuteProcedure(string name, object parameters)
        {
            conexao.Open();
            conexao.Execute(name, parameters, commandType: CommandType.StoredProcedure, commandTimeout: TimeOut);
            conexao.Close();
        }

        public T ExecuteProcedureScalar<T>(string name, object parameters)
        {
            conexao.Open();
            var retorno = conexao.Query<T>(name, parameters, commandType: CommandType.StoredProcedure, commandTimeout: TimeOut).FirstOrDefault();
            conexao.Close();
            return (T)retorno;
        }

        public List<T> ExecuteProcedure<T>(string name)
        {
            conexao.Open();
            var retorno = conexao.Query<T>(name, commandType: CommandType.StoredProcedure, commandTimeout: TimeOut);
            conexao.Close();
            return (List<T>)retorno;
        }

        public List<T> ExecuteProcedure<T>(string name, object parameters)
        {
            conexao.Open();
            var retorno = conexao.Query<T>(name, parameters, commandType: CommandType.StoredProcedure, commandTimeout: TimeOut);
            conexao.Close();
            return (List<T>)retorno;
        }

        public void Dispose()
        {
            conexao.Dispose();
        }


        public List<T> GetAll<T>()
        {
            conexao.Open();
            var retorno = conexao.Query<T>(string.Format("select * from {0}", typeof(T).Name), commandTimeout: TimeOut);
            conexao.Close();
            return (List<T>)retorno;

        }

        public List<T> Get<T>(string tableName, object parameters)
        {


            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("select * from {0}", tableName);
            var fisrtItem = parameters.GetType().GetProperties().FirstOrDefault();
            foreach (PropertyInfo item in parameters.GetType().GetProperties())
            {
                var value = (item.GetValue(parameters, null).GetType()).Name == "String" ? "'" + item.GetValue(parameters, null) + "'" : item.GetValue(parameters, null);
                sb.AppendFormat(" {0} {1} = {2}", (item == fisrtItem ? "WHERE" : "AND"), item.Name, value);
            }

            conexao.Open();
            var retorno = conexao.Query<T>(sb.ToString(), commandTimeout: TimeOut);
            conexao.Close();

            return (List<T>)retorno;
        }


        public void Update(object parametersSet)
        {
            //update ResourceClass set  valuept = N'Ativo?' ,ValueCh = N'活躍' where ResourceClassId = 1
            var idObjet = parametersSet.GetType().GetProperties().Where(c => c.Name == parametersSet.GetType().Name + "Id").FirstOrDefault();

            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("update {0}", parametersSet.GetType().Name);
            bool first = true;
            Int64 id = 0;
            foreach (var item in parametersSet.GetType().GetProperties())
            {
                if (item.Name == idObjet.Name)
                {
                    id = Int64.Parse(item.GetValue(parametersSet, null).ToString());
                    continue;
                }
                var value = (item.GetValue(parametersSet, null).GetType()).Name == "String" ? "N'" + item.GetValue(parametersSet, null) + "'" : item.GetValue(parametersSet, null);
                sb.AppendFormat(" {0} {1} = {2}", (first ? "set" : ", "), item.Name, value);
                first = false;
            }

            sb.AppendFormat(" {0} {1} = {2}", "WHERE", idObjet.Name, id);


            conexao.Open();
            var retorno = conexao.Query(sb.ToString(), commandTimeout: TimeOut);
            conexao.Close();
        }

        public List<T> Get<T>(object parameters)
        {


            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("select * from {0}", typeof(T).Name);
            var fisrtItem = parameters.GetType().GetProperties().FirstOrDefault();
            foreach (PropertyInfo item in parameters.GetType().GetProperties())
            {
                var value = new Object();
                switch (item.GetValue(parameters, null).GetType().Name)
                {
                    case "String":
                    case "Char":
                    case "Guid":
                        value = "'" + item.GetValue(parameters, null) + "'";
                        break;
                    default:
                        value = AjustaValor(item.GetValue(parameters, null));
                        break;
                }
                sb.AppendFormat(" {0} {1} = {2}", (item == fisrtItem ? "WHERE" : "AND"), item.Name, value);
            }


            try
            {
                conexao.Open();
                var retorno = conexao.Query<T>(sb.ToString(), commandTimeout: TimeOut);
                conexao.Close();
                return (List<T>)retorno;
            }
            catch (Exception erro)
            {
                throw new Exception("Erro de Conexao Dapper SQL " + conexao + "\n" + erro.Message);
            }
        }

        private object AjustaValor(object valor)
        {

            if (valor.GetType().Name == "Boolean")
            {
                if (valor.ToString() == "False")
                    return "0";
                else
                    if (valor.ToString() == "True")
                    return "1";

            }

            return valor;
        }


        public bool TesteConexao()
        {
            bool retorno = false;
            try
            {
                conexao.Open();
                retorno = true;
                conexao.Close();
            }
            catch (Exception)
            {
            }
            return retorno;
        }


    }
}
