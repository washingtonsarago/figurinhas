﻿using Figurinhas.Application.Interfaces;
using Figurinhas.Domain.Entities;
using Ninject;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Figurinhas.MVC.Controllers
{
    public class FigurinhasController : Controller
    {
        [Inject]
        public IFigurinhaApplication figurinhaApplication { get; set; }

        [Inject]
        public ITrocaApplication trocaApplication { get; set; }

        // GET: Figurinhas
        public ActionResult Index()
        {
            Usuario usuario = (Usuario)Session["Usuario"];
            if (usuario == null)
                return RedirectToActionPermanent("Index", "Home");

            List<Figurinha> lFigurinhas = figurinhaApplication.BuscarPorUsuario(usuario.UsuarioId);

            return View(lFigurinhas);
        }

        public ActionResult Create()
        {
            Usuario usuario = (Usuario)Session["Usuario"];
            if (usuario == null)
                return RedirectToActionPermanent("Index", "Home");

            return View();

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Figurinha figurinha)
        {
            Usuario usuario = (Usuario)Session["Usuario"];
            if (usuario == null)
                return RedirectToActionPermanent("Index", "Home");


            Figurinha fig = figurinhaApplication.BuscarPorNumeroEUsuario(figurinha.NumeroDaFigurinha, usuario.UsuarioId);
            //Verificando se já existe uma figurinha com esse número, caso tenha ele acresnta a quantidade na figurinha existente, evitando a duplicidade

            if (fig != null)
            {
                if (fig.isAtivo)
                    fig.Total += figurinha.Total;
                else
                {
                    fig.isAtivo = true;
                    fig.Total = figurinha.Total;
                }
                figurinhaApplication.Update(fig);
            }
            //caso negativo é adcionado normalmente
            else
            {
                try
                {
                    figurinha.Reservada = 0;
                    figurinha.isAtivo = true;
                    figurinha.UsuarioId = usuario.UsuarioId;
                    figurinhaApplication.Add(figurinha);
                }

                catch (Exception)
                {
                    return View(figurinha);
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            Usuario usuario = (Usuario)Session["Usuario"];
            if (usuario == null)
                return RedirectToActionPermanent("Index", "Home");
            Figurinha fig = figurinhaApplication.GetById(id);

            //com o intuito de evitar que outros usuarios editem as figurinhas dos outros é feito uma checagem para ver se o usuário realmente pode editar a figurinha
            if (fig.UsuarioId != usuario.UsuarioId)
                return RedirectToAction("Index");

            return View(fig);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Figurinha figurinha)
        {
            try
            {
                Usuario usuario = (Usuario)Session["Usuario"];
                if (usuario == null)
                    return RedirectToActionPermanent("Index", "Home");

                Figurinha fig = figurinhaApplication.GetById(figurinha.FigurinhaId);
                Figurinha figUpDate = figurinhaApplication.GetById(figurinha.FigurinhaId);


                if (fig.UsuarioId != usuario.UsuarioId)
                    return RedirectToAction("Index");

                //Caso o usuário troque o número da figurinha o sistema cancela todas as solicitações de trocas para o número anterior.
                if (figurinha.NumeroDaFigurinha != fig.NumeroDaFigurinha)
                {
                    List<Troca> lTrocas = trocaApplication.BuscarTrocasPendentes(fig.FigurinhaId);
                    Figurinha figurinhaSolicitada;
                    Figurinha figurinhaSolicitante;

                    foreach (var item in lTrocas)
                    {
                        figurinhaSolicitada = new Figurinha();
                        figurinhaSolicitada = figurinhaApplication.GetById(item.FigurinhaSaidaId);
                        figurinhaSolicitada.Reservada--;
                        figurinhaApplication.Update(figurinhaSolicitada);

                        figurinhaSolicitante = new Figurinha();
                        figurinhaSolicitante = figurinhaApplication.GetById(item.FigurinhaEntradaId);
                        figurinhaSolicitante.Reservada--;
                        figurinhaApplication.Update(figurinhaSolicitante);

                        trocaApplication.Remove(item);
                    }
                    figUpDate.NumeroDaFigurinha = figurinha.NumeroDaFigurinha;
                    figUpDate.Reservada = 0;

                    fig = figurinhaApplication.BuscarPorNumeroEUsuario(figurinha.NumeroDaFigurinha, usuario.UsuarioId);

                    if (fig != null && fig.FigurinhaId != figUpDate.FigurinhaId)
                    {
                        if (fig.isAtivo)
                            fig.Total += figurinha.Total;
                        else
                        {
                            fig.isAtivo = true;
                            fig.Total = figurinha.Total;
                        }
                        figUpDate.isAtivo = false;
                        figurinhaApplication.Update(fig);
                    }
                }

                figUpDate.Total = figurinha.Total;

                //Se ele inseriu um número que ele já possui o é feito a somatória e exluido logicamente a figurinha editada e add a quantidade na outra figurinha com o mesmo número
               


                figurinhaApplication.Update(figUpDate);

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View(figurinha);
            }
        }

        public ActionResult Delete(int id)
        {
            Usuario usuario = (Usuario)Session["Usuario"];
            if (usuario == null)
                return RedirectToActionPermanent("Index", "Home");
            Figurinha fig = figurinhaApplication.GetById(id);

            //Verificação se o usuário pode apagar
            if (fig.UsuarioId != usuario.UsuarioId)
                return RedirectToAction("Index");

            return View(fig);

        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(int id, Troca troca)
        {
            Usuario usuario = (Usuario)Session["Usuario"];
            if (usuario == null)
                return RedirectToActionPermanent("Index", "Home");
            //Verificação se o usuário pode apagar
            Figurinha fig = figurinhaApplication.GetById(id);
            if (fig.UsuarioId != usuario.UsuarioId)
                return RedirectToAction("Index");

            Figurinha figurinhaSolicitada;
            Figurinha figurinhaSolicitante;

            //Removendo trocas pendentes caso exista
            List<Troca> lTrocas = trocaApplication.BuscarTrocasPendentes(id);
            foreach (var item in lTrocas)

            {
                figurinhaSolicitada = new Figurinha();
                figurinhaSolicitada = figurinhaApplication.GetById(item.FigurinhaSaidaId);
                figurinhaSolicitada.Reservada--;
                figurinhaApplication.Update(figurinhaSolicitada);


                figurinhaSolicitante = new Figurinha();
                figurinhaSolicitante = figurinhaApplication.GetById(item.FigurinhaEntradaId);
                figurinhaSolicitante.Reservada--;
                figurinhaApplication.Update(figurinhaSolicitante);

                trocaApplication.Remove(item);
            }

            fig.isAtivo = false;
            figurinhaApplication.Update(fig);

            return RedirectToAction("Index");
        }
    }
}