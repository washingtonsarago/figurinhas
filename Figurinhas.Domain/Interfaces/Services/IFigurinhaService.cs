﻿using Figurinhas.Domain.Entities;

namespace Figurinhas.Domain.Interfaces.Services
{
    public interface IFigurinhaService : IServiceBase<Figurinha>
    {
      
    }
}
