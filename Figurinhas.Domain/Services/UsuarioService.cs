﻿using Figurinhas.Domain.Entities;
using Figurinhas.Domain.Interfaces.Respositories;
using Figurinhas.Domain.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace Figurinhas.Domain.Services
{
    public class UsuarioService : ServiceBase<Usuario>, IUsuarioService
    {

        private readonly IUsuarioRepository usuarioRepository;

        public UsuarioService(IUsuarioRepository usuarioRepository) : base(usuarioRepository)
        {
            this.usuarioRepository = usuarioRepository;
        }

        public List<Usuario> RankingTrocas()
        {
            return usuarioRepository.GetAll().ToList();
        }

        public Usuario VerificarSeUsuarioEstadisponivel(string usuario)
        {
            return usuarioRepository.GetAll().Where(u => u.Nome == usuario.ToLower()).FirstOrDefault();
        }

        public Usuario VerificarUsuarioESenha(string usuario, string senha)
        {
            return usuarioRepository.GetAll().Where(c => c.Nome == usuario.ToLower() && c.Senha == senha).FirstOrDefault();

        }
    }
}
