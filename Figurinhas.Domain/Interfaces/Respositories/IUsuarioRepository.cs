﻿using Figurinhas.Domain.Entities;

namespace Figurinhas.Domain.Interfaces.Respositories
{
    public interface IUsuarioRepository : IRepositoryBase<Usuario>
    {
    }
}
